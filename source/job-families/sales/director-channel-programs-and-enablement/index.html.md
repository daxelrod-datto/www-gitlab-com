---
layout: job_family_page
title: "Director, Channel Programs & Enablement"
---

The Director of Partner Programs & Enablement helps manage and support the growth of the channel business. They are responsible for the day to day channel program development/operation as well as the execution of our partner program. The Director, Channel Programs and Enablement reports into the VP, Channel Sales and works cross-functionally with marketing, sales, product, channel ops, and legal to integrate channel programs throughout the business.

## Responsibilities

* Responsible for the design, build and management of partner programs to support Gitlab Channel working in collaboration with the Channel Sales team
* Develop Partner Program collateral/documents and onboarding tools to enhance overall partner experience and communicate to partners
* Develop Partner Portal; leveraging SFDC & PRM - working with marketing & Channel Ops 
* Provide management of Partner Portal to create an engaging platform for partners to interact with Gitlab (lead registration, opportunity tracking, commissions, etc)
* Work with channel operations/IT to implement channel strategy, data/reporting, system workflows, and overall operational governance for the program
* Design Channel Enablement Certification & Badge Model.  Work closely with Channel Marketing, Channel Services, Customer Success & Sales enablement on partner enablement tools/training and implement as part of overall program
* Manage and provide administration of partner agreements/partner contracting working in coordination with legal
* Communicate channel programs, strategies & events and updates internally to drive alignment throughout the business
* Work cross-functionally with marketing, product, legal and finance to continue to implement and grow partner programs and ensure alignment across the business
* Work with partners to solicit feedback on existing programs/processes and implement changes (Partner Advisory Councils, via channel teams, etc) 
* Build a team to effectively manage channel programs, enablement, portal, comms & MDF/incentive management.  

## Requirements

* Prior experience in running Partner Programs, Channel Enablement, Channel Marketing and/or Channel Development in the services/IT space
* Thrive in a fast-paced, ever-changing environment
* Problem solver and self-motivated
* Exceptional oral and written communication and presentation skills required
* Strong interpersonal skills
* Ability to conceptualize and sell ideas internally
* Excellent time-management and multi-tasking abilities
* Innovation and the ability to gain consensus is necessary
* Works extremely well as a member of a team, but also excels as an individual contributor
* You share our values, and work in accordance with those values.
* [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#director-group)
* Ability to use GitLab

## Performance Indicators
[Sales KPI's](https://about.gitlab.com/handbook/business-ops/data-team/kpi-index/#sales-kpis)

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process.
1. Phone screen with a GitLab Recruiting team memeber 
2. Video Interview with the Hiring Manager
3. Team Interviews with 1-4 teammates 
Additional details about our process can be found on our [hiring page](/handbook/hiring).
